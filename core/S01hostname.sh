#! /bin/sh

PATH=/sbin:/bin

. /lib/init/vars.sh
. /lib/lsb/init-functions

[ -f /etc/hostname ] || return
HOSTNAME="$(cat /etc/hostname)"

[ "$VERBOSE" != no ] && log_action_begin_msg "Setting hostname to '$HOSTNAME'"
hostname "$HOSTNAME"
ES=$?
[ "$VERBOSE" != no ] && log_action_end_msg $ES
exit $ES
