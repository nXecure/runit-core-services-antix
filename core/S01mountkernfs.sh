#! /bin/sh

PATH=/sbin:/bin
. /lib/init/vars.sh
. /lib/init/tmpfs.sh

. /lib/lsb/init-functions
. /lib/init/mount-functions.sh

# May be run several times, so must be idempotent.
# $1: Mount mode, to allow for remounting
MNTMODE="$1"

#
# Mount tmpfs on /run and/or /run/lock
#
mount_run "$MNTMODE"
mount_lock "$MNTMODE"

#
# Mount proc filesystem on /proc
#
domount "$MNTMODE" proc "" /proc proc "-onodev,noexec,nosuid"

if grep -E -qs "securityfs\$" /proc/filesystems ; then
	domount "$MNTMODE" securityfs "" /sys/kernel/security securityfs
fi

#
# Mount sysfs on /sys
#
# Only mount sysfs if it is supported (kernel >= 2.6)
if grep -E -qs "sysfs\$" /proc/filesystems
then
	domount "$MNTMODE" sysfs "" /sys sysfs "-onodev,noexec,nosuid"
fi

if [ -d /sys/fs/pstore ]
then
	domount "$MNTMODE" pstore "" /sys/fs/pstore pstore ""
fi

